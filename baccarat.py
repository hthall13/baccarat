import csv
import json
import argparse
import numpy as np
from random import shuffle
from itertools import product
import matplotlib.pyplot as plt
import strategies

class Baccarat:
  # a list of all the suits
  SUITS = ["\u2663", "\u2665", "\u2666", "\u2660"]
  # a list of all the ranks
  RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

  SHOE = []

  SCORE = []

  def __init__(self, shoe_size):
    for deck in range(shoe_size):
      self.SHOE.extend(list(product(self.RANKS, self.SUITS)))

  def shuffle(self):
    shuffle(self.SHOE)

  def redeal(self, shoe_size):
    self.SHOE = []
    self.SCORE = []
    self.__init__(shoe_size)
    self.shuffle()

  def value(self, card):
    rank = card[0]
    if rank == 'A':
      value = 1
    elif rank not in ['J', 'Q', 'K']:
      value = int(rank)
    else:
      value = 10
    return value

  def bvalue(self, value):
    while value >= 10:
      value = value - 10
    return value

  def draw(self):
    return self.SHOE.pop(0)

  def start(self):
    first_card = self.draw()
    for i in range(self.value(first_card)):
      self.draw()

  def deal(self, printed=False):
    note = ""
    player_first = self.draw()
    banker_first = self.draw()
    player_second = self.draw()
    banker_second = self.draw()
    player_third = None
    banker_third = None
    player = self.value(player_first) + self.value(player_second)
    banker = self.value(banker_first) + self.value(banker_second)
    # natural
    if self.bvalue(banker) >= 8:
      note = "banker natural"
    else:
      if self.bvalue(player) >= 8:
        note = "player natural"
      if self.bvalue(player) <= 5:
        player_third = self.draw()
        if self.value(player_third) >= 9:
          if self.bvalue(banker) <= 3:
            banker_third = self.draw()
            banker += self.value(banker_third)
        elif self.value(player_third) == 8:
          if self.bvalue(banker) <= 2:
            banker_third = self.draw()
            banker += self.value(banker_third)
        elif self.value(player_third) >= 6:
          if self.bvalue(banker) <= 6:
            banker_third = self.draw()
            banker += self.value(banker_third)
        elif self.value(player_third) >= 4:
          if self.bvalue(banker) <= 5:
            banker_third = self.draw()
            banker += self.value(banker_third)
        elif self.value(player_third) >= 2:
          if self.bvalue(banker) <= 4:
            banker_third = self.draw()
            banker += self.value(banker_third)
        player += self.value(player_third)
      else:
        if self.bvalue(banker) <= 5:
          banker_third = self.draw()
          banker += self.value(banker_third)
    if printed:
      if player_third is not None and banker_third is not None:
        print(" player (%d)    banker (%d) %s" % (self.bvalue(player), self.bvalue(banker), note))
        print("%s %s %s      %s %s %s" % (
          player_first[0] + player_first[1],
          player_second[0] + player_second[1],
          player_third[0] + player_third[1],
          banker_first[0] + banker_first[1],
          banker_second[0] + banker_second[1],
          banker_third[0] + banker_third[1]
          )
        )
      elif player_third is not None:
        print(" player (%d)    banker (%d) %s" % (self.bvalue(player), self.bvalue(banker), note))
        print("%s %s %s      %s    %s" % (
          player_first[0] + player_first[1],
          player_second[0] + player_second[1],
          player_third[0] + player_third[1],
          banker_first[0] + banker_first[1],
          banker_second[0] + banker_second[1]
          )
        )
      elif banker_third is not None:
        print(" player (%d)    banker (%d) %s" % (self.bvalue(player), self.bvalue(banker), note))
        print("%s    %s      %s %s %s" % (
          player_first[0] + player_first[1],
          player_second[0] + player_second[1],
          banker_first[0] + banker_first[1],
          banker_second[0] + banker_second[1],
          banker_third[0] + banker_third[1]
          )
        )
      else:
        print(" player (%d)    banker (%d) %s" % (self.bvalue(player), self.bvalue(banker), note))
        print("%s    %s      %s    %s" % (
          player_first[0] + player_first[1],
          player_second[0] + player_second[1],
          banker_first[0] + banker_first[1],
          banker_second[0] + banker_second[1]
          )
        )
    if self.bvalue(banker) > self.bvalue(player): winner = "banker"
    elif self.bvalue(banker) < self.bvalue(player): winner = "player"
    else: winner = "tie"
    self.SCORE.append(winner)
    return winner

  def play(self, deals):
    while deals > 0:
      game.deal()
      deals -= 1

if __name__ == '__main__':

  data = {
    "Donkey (Banker)": {"totals": []},
    "Martingale (Banker)": {"totals": []},
    "Fibonacci (Banker)": {"totals": []},
    "Paroli (Banker)": {"totals": []},
    "D'Alembert (Banker)": {"totals": []},
    "Labouchère (Banker)": {"totals": []},

    "Donkey (Player)": {"totals": []},
    "Martingale (Player)": {"totals": []},
    "Fibonacci (Player)": {"totals": []},
    "Paroli (Player)": {"totals": []},
    "D'Alembert (Player)": {"totals": []},
    "Labouchère (Player)": {"totals": []},

    "Donkey (Winner)": {"totals": []},
    "Martingale (Winner)": {"totals": []},
    "Fibonacci (Winner)": {"totals": []},
    "Paroli (Winner)": {"totals": []},
    "D'Alembert (Winner)": {"totals": []},
    "Labouchère (Winner)": {"totals": []},

    "Donkey (Chop)": {"totals": []},
    "Martingale (Chop)": {"totals": []},
    "Fibonacci (Chop)": {"totals": []},
    "Paroli (Chop)": {"totals": []},
    "D'Alembert (Chop)": {"totals": []},
    "Labouchère (Chop)": {"totals": []},

    "Reverse Donkey (Banker)": {"totals": []},
    "Reverse Martingale (Banker)": {"totals": []},
    "Reverse Fibonacci (Banker)": {"totals": []},
    "Reverse Paroli (Banker)": {"totals": []},
    "Reverse D'Alembert (Banker)": {"totals": []},
    "Reverse Labouchère (Banker)": {"totals": []},

    "Reverse Donkey (Player)": {"totals": []},
    "Reverse Martingale (Player)": {"totals": []},
    "Reverse Fibonacci (Player)": {"totals": []},
    "Reverse Paroli (Player)": {"totals": []},
    "Reverse D'Alembert (Player)": {"totals": []},
    "Reverse Labouchère (Player)": {"totals": []},

    "Reverse Donkey (Winner)": {"totals": []},
    "Reverse Martingale (Winner)": {"totals": []},
    "Reverse Fibonacci (Winner)": {"totals": []},
    "Reverse Paroli (Winner)": {"totals": []},
    "Reverse D'Alembert (Winner)": {"totals": []},
    "Reverse Labouchère (Winner)": {"totals": []},

    "Reverse Donkey (Chop)": {"totals": []},
    "Reverse Martingale (Chop)": {"totals": []},
    "Reverse Fibonacci (Chop)": {"totals": []},
    "Reverse Paroli (Chop)": {"totals": []},
    "Reverse D'Alembert (Chop)": {"totals": []},
    "Reverse Labouchère (Chop)": {"totals": []},

    "CEG Dealer School": {"totals": []},
    "Knightingale": {"totals": []},
    "Henri D'Alembert": {"totals": []},
    "Pok's Method": {"totals": []},
    "Lauren's Method": {"totals": []},
    "James Bond": {"totals": []}
  }

  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

  parser.add_argument('-s', '--strategy',
    type=str,
    default="all",
    # choices=list(data.keys()),
    choices=["all", "standard", "reverse", "custom"],
    help="pick the baccarat strategies to test")
  parser.add_argument('-p', '--print',
    action='store_true',
    default=True,
    help="print the results of the tests")
  parser.add_argument('-a', '--analyze',
    type=str,
    choices=["csv", "json", "all"],
    default=None,
    help="save the test analysis to a output file")
  parser.add_argument('-o', '--output',
    type=str,
    choices=["csv", "json", "all"],
    default=None,
    help="save the complete test data from each shoe to an output file")
  parser.add_argument('-g', '--graph',
    action='store_true',
    default=False,
    help="graph the results of the tests")
  parser.add_argument('-b', '--buy_in',
    type=int,
    default=500,
    required=False,
    help="amount to buy into the shoe")
  parser.add_argument('-u', '--unit',
    type=int,
    default=25,
    required=False,
    help="starting bet or unit of betting")
  parser.add_argument('-d', '--deals',
    type=int,
    default=50,
    required=False,
    help="number of deals per shoe")
  parser.add_argument('-D', '--decks',
    type=int,
    default=8,
    required=False,
    help="number of decks in shoe")
  parser.add_argument('-t', '--trials',
    type=int,
    default=1000,
    required=False,
    help="number of trials in test")
  args = parser.parse_args()


  game = Baccarat(args.decks)
  game.shuffle()
  complete = 0
  print("Running strategies...")
  for i in range(args.trials):

    if i > 0:
      game.redeal(args.decks)
    game.play(args.deals)

    if args.strategy == "all" or args.strategy == "standard":
      dunce = strategies.donkey(game.SCORE, args.buy_in, args.unit)
      john = strategies.martingale(game.SCORE, args.buy_in, args.unit)
      leonardo = strategies.fibonacci(game.SCORE, args.buy_in, args.unit)
      lucius = strategies.paroli(game.SCORE, args.buy_in, args.unit)
      jean = strategies.d_alembert(game.SCORE, args.buy_in, args.unit)
      henri = strategies.labouchere(game.SCORE, args.buy_in, args.unit)
      data["Donkey (Banker)"]["totals"].append(dunce[-1])
      data["Martingale (Banker)"]["totals"].append(john[-1])
      data["Fibonacci (Banker)"]["totals"].append(leonardo[-1])
      data["Paroli (Banker)"]["totals"].append(lucius[-1])
      data["D'Alembert (Banker)"]["totals"].append(jean[-1])
      data["Labouchère (Banker)"]["totals"].append(henri[-1])

      dunce_p = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="player")
      john_p = strategies.martingale(game.SCORE, args.buy_in, args.unit, pose="player")
      leonardo_p = strategies.fibonacci(game.SCORE, args.buy_in, args.unit, pose="player")
      lucius_p = strategies.paroli(game.SCORE, args.buy_in, args.unit, pose="player")
      jean_p = strategies.d_alembert(game.SCORE, args.buy_in, args.unit, pose="player")
      henri_p = strategies.labouchere(game.SCORE, args.buy_in, args.unit, pose="player")
      data["Donkey (Player)"]["totals"].append(dunce_p[-1])
      data["Martingale (Player)"]["totals"].append(john_p[-1])
      data["Fibonacci (Player)"]["totals"].append(leonardo_p[-1])
      data["Paroli (Player)"]["totals"].append(lucius_p[-1])
      data["D'Alembert (Player)"]["totals"].append(jean_p[-1])
      data["Labouchère (Player)"]["totals"].append(henri_p[-1])

      dunce_w = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="winner")
      john_w = strategies.martingale(game.SCORE, args.buy_in, args.unit, pose="winner")
      leonardo_w = strategies.fibonacci(game.SCORE, args.buy_in, args.unit, pose="winner")
      lucius_w = strategies.paroli(game.SCORE, args.buy_in, args.unit, pose="winner")
      jean_w = strategies.d_alembert(game.SCORE, args.buy_in, args.unit, pose="winner")
      henri_w = strategies.labouchere(game.SCORE, args.buy_in, args.unit, pose="winner")
      data["Donkey (Winner)"]["totals"].append(dunce_w[-1])
      data["Martingale (Winner)"]["totals"].append(john_w[-1])
      data["Fibonacci (Winner)"]["totals"].append(leonardo_w[-1])
      data["Paroli (Winner)"]["totals"].append(lucius_w[-1])
      data["D'Alembert (Winner)"]["totals"].append(jean_w[-1])
      data["Labouchère (Winner)"]["totals"].append(henri_w[-1])

      dunce_c = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="chop")
      john_c = strategies.martingale(game.SCORE, args.buy_in, args.unit, pose="chop")
      leonardo_c = strategies.fibonacci(game.SCORE, args.buy_in, args.unit, pose="chop")
      lucius_c = strategies.paroli(game.SCORE, args.buy_in, args.unit, pose="chop")
      jean_c = strategies.d_alembert(game.SCORE, args.buy_in, args.unit, pose="chop")
      henri_c = strategies.labouchere(game.SCORE, args.buy_in, args.unit, pose="chop")
      data["Donkey (Chop)"]["totals"].append(dunce_c[-1])
      data["Martingale (Chop)"]["totals"].append(john_c[-1])
      data["Fibonacci (Chop)"]["totals"].append(leonardo_c[-1])
      data["Paroli (Chop)"]["totals"].append(lucius_c[-1])
      data["D'Alembert (Chop)"]["totals"].append(jean_c[-1])
      data["Labouchère (Chop)"]["totals"].append(henri_c[-1])

    if args.strategy == "all" or args.strategy == "reverse":
      ecnud = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="player")
      nhoj = strategies.reverse_martingale(game.SCORE, args.buy_in, args.unit)
      odranoel = strategies.reverse_fibonacci(game.SCORE, args.buy_in, args.unit)
      suicul = strategies.reverse_paroli(game.SCORE, args.buy_in, args.unit)
      naej = strategies.reverse_d_alembert(game.SCORE, args.buy_in, args.unit)
      irneh = strategies.reverse_labouchere(game.SCORE, args.buy_in, args.unit)
      data["Reverse Donkey (Banker)"]["totals"].append(ecnud[-1])
      data["Reverse Martingale (Banker)"]["totals"].append(nhoj[-1])
      data["Reverse Fibonacci (Banker)"]["totals"].append(odranoel[-1])
      data["Reverse Paroli (Banker)"]["totals"].append(suicul[-1])
      data["Reverse D'Alembert (Banker)"]["totals"].append(naej[-1])
      data["Reverse Labouchère (Banker)"]["totals"].append(irneh[-1])

      ecnud_p = strategies.donkey(game.SCORE, args.buy_in, args.unit)
      nhoj_p = strategies.reverse_martingale(game.SCORE, args.buy_in, args.unit, pose="player")
      odranoel_p = strategies.reverse_fibonacci(game.SCORE, args.buy_in, args.unit, pose="player")
      suicul_p = strategies.reverse_paroli(game.SCORE, args.buy_in, args.unit, pose="player")
      naej_p = strategies.reverse_d_alembert(game.SCORE, args.buy_in, args.unit, pose="player")
      irneh_p = strategies.reverse_labouchere(game.SCORE, args.buy_in, args.unit, pose="player")
      data["Reverse Donkey (Player)"]["totals"].append(ecnud_p[-1])
      data["Reverse Martingale (Player)"]["totals"].append(nhoj_p[-1])
      data["Reverse Fibonacci (Player)"]["totals"].append(odranoel_p[-1])
      data["Reverse Paroli (Player)"]["totals"].append(suicul_p[-1])
      data["Reverse D'Alembert (Player)"]["totals"].append(naej_p[-1])
      data["Reverse Labouchère (Player)"]["totals"].append(irneh_p[-1])

      ecnud_w = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="winner")
      nhoj_w = strategies.reverse_martingale(game.SCORE, args.buy_in, args.unit, pose="winner")
      odranoel_w = strategies.reverse_fibonacci(game.SCORE, args.buy_in, args.unit, pose="winner")
      suicul_w = strategies.reverse_paroli(game.SCORE, args.buy_in, args.unit, pose="winner")
      naej_w = strategies.reverse_d_alembert(game.SCORE, args.buy_in, args.unit, pose="winner")
      irneh_w = strategies.reverse_labouchere(game.SCORE, args.buy_in, args.unit, pose="winner")
      data["Reverse Donkey (Winner)"]["totals"].append(ecnud_w[-1])
      data["Reverse Martingale (Winner)"]["totals"].append(nhoj_w[-1])
      data["Reverse Fibonacci (Winner)"]["totals"].append(odranoel_w[-1])
      data["Reverse Paroli (Winner)"]["totals"].append(suicul_w[-1])
      data["Reverse D'Alembert (Winner)"]["totals"].append(naej_w[-1])
      data["Reverse Labouchère (Winner)"]["totals"].append(irneh_w[-1])

      ecnud_c = strategies.donkey(game.SCORE, args.buy_in, args.unit, pose="chop")
      nhoj_c = strategies.reverse_martingale(game.SCORE, args.buy_in, args.unit, pose="chop")
      odranoel_c = strategies.reverse_fibonacci(game.SCORE, args.buy_in, args.unit, pose="chop")
      suicul_c = strategies.reverse_paroli(game.SCORE, args.buy_in, args.unit, pose="chop")
      naej_c = strategies.reverse_d_alembert(game.SCORE, args.buy_in, args.unit, pose="chop")
      irneh_c = strategies.reverse_labouchere(game.SCORE, args.buy_in, args.unit, pose="chop")
      data["Reverse Donkey (Chop)"]["totals"].append(ecnud_c[-1])
      data["Reverse Martingale (Chop)"]["totals"].append(nhoj_c[-1])
      data["Reverse Fibonacci (Chop)"]["totals"].append(odranoel_c[-1])
      data["Reverse Paroli (Chop)"]["totals"].append(suicul_c[-1])
      data["Reverse D'Alembert (Chop)"]["totals"].append(naej_c[-1])
      data["Reverse Labouchère (Chop)"]["totals"].append(irneh_c[-1])

    if args.strategy == "all" or args.strategy == "custom":
      timmy = strategies.ceg_dealer_school(game.SCORE, args.buy_in, args.unit)
      mr_gullible = strategies.knightingale(game.SCORE, args.buy_in, args.unit)
      henry = strategies.henri_d_alembert(game.SCORE, args.buy_in, args.unit)
      korntanat = strategies.pok(game.SCORE, args.buy_in, args.unit)
      lauren = strategies.laurens_method(game.SCORE, args.buy_in, args.unit)
      james = strategies.james_bond(game.SCORE, args.buy_in, args.unit)
      data["CEG Dealer School"]["totals"].append(timmy[-1])
      data["Knightingale"]["totals"].append(mr_gullible[-1])
      data["Henri D'Alembert"]["totals"].append(henry[-1])
      data["Pok's Method"]["totals"].append(korntanat[-1])
      data["Lauren's Method"]["totals"].append(lauren[-1])
      data["James Bond"]["totals"].append(james[-1])

    new = int((i / args.trials) * 100)
    if complete != new:
      print("%d%% complete..." % (new))
      complete = new

  color = 0
  colors = [
    "tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
    "tab:brown", "tab:pink", "tab:gray", "tab:olive", "tab:cyan"
  ]
  results ={
    "strats": [], "means": [], "maxs": [], "colors": [],
    "ups": [], "flats": [], "downs": [], "busts": [],
    "ups_p": [], "flats_p": [], "downs_p": [], "busts_p": []
  }
  for strategy, info in data.items():
    if len(info["totals"]) > 0:
      mean = round(sum(info["totals"]) / len(info["totals"]), 2) 
      max_val = round(max(info["totals"]), 2)
      ups = 0
      flats = 0
      downs = 0
      busts = 0
      for value in info["totals"]:
        if value == args.buy_in:
          flats += 1
        elif value > args.buy_in:
          ups += 1
        elif value <= 0:
          busts += 1
        elif value < args.buy_in:
          downs += 1
      p_ups = round((ups / len(info["totals"])) * 100, 2)
      p_flats = round((flats / len(info["totals"])) * 100, 2)
      p_downs = round((downs / len(info["totals"])) * 100, 2)
      p_busts = round((busts / len(info["totals"])) * 100, 2)
      results["strats"].append(strategy)
      results["means"].append(mean)
      results["maxs"].append(max_val)
      results["colors"].append(colors[color])
      results["ups"].append(ups)
      results["ups_p"].append(p_ups)
      results["flats"].append(flats)
      results["flats_p"].append(p_flats)
      results["downs"].append(downs)
      results["downs_p"].append(p_downs)
      results["busts"].append(busts)
      results["busts_p"].append(p_busts)
      if color <= 8:
        color += 1
      else:
        color = 0
      if args.print:
        while len(strategy) < 30:
          strategy = strategy + " "
        print(strategy + ": Average finish: %.2f,   ups: %d (%.2f%%),   downs: %d (%.2f%%),   busts: %d (%.2f%%)" % (mean, ups, p_ups, downs, p_downs, busts, p_busts))

  if args.graph:
    if args.print:
      print("generating average chart...")
    buys = np.full(len(results["strats"]), args.buy_in)
    plt.figure(figsize=(16, 8))
    plt.bar(results["strats"], results["means"], color=results["colors"])
    plt.plot(buys, color="red")
    plt.title("Average Payouts")
    plt.ylabel("Average Total ($)")
    plt.ylim([0, args.buy_in * 1.5])
    plt.tick_params(axis='x', labelrotation=40)
    plt.xticks(ha='right')
    plt.tight_layout()
    plt.savefig('public/averages.png')

    if args.print:
      print("generating percentage chart...")
    bottom = np.zeros(len(results["strats"]))
    reds = np.full(len(results["strats"]), "tab:red")
    oranges = np.full(len(results["strats"]), "tab:orange")
    greens = np.full(len(results["strats"]), "tab:green")
    blues = np.full(len(results["strats"]), "tab:blue")
    quarters = np.full(len(results["strats"]), 25)
    halves = np.full(len(results["strats"]), 50)
    triquarters = np.full(len(results["strats"]), 75)
    plt.figure(figsize=(16, 8))
    plt.bar(results["strats"], results["busts_p"], color=reds, bottom=bottom)
    bottom += results["busts_p"]
    plt.bar(results["strats"], results["downs_p"], color=oranges, bottom=bottom)
    bottom += results["downs_p"]
    plt.bar(results["strats"], results["flats_p"], color=greens, bottom=bottom)
    bottom += results["flats_p"]
    plt.bar(results["strats"], results["ups_p"], color=blues, bottom=bottom)
    plt.plot(quarters, color="black")
    plt.plot(halves, color="black")
    plt.plot(triquarters, color="black")
    plt.title("Distribution")
    plt.ylabel("Busts vs. Downs vs. Flats vs. Ups")
    plt.ylim([0, 100])
    plt.tick_params(axis='x', labelrotation=40)
    plt.xticks(ha='right')
    plt.tight_layout()
    plt.savefig('public/percents.png')

    if args.strategy == "all" or args.strategy == "standard":
      if args.print:
        print("generating standard strategy chart...")
      plt.figure(figsize=(16, 8))
      plt.subplot(221)
      plt.plot(dunce, label="Donkey")
      plt.plot(john, label="Martingale")
      plt.plot(leonardo, label="Fibonacci")
      plt.plot(lucius, label="Paroli")
      plt.plot(jean, label="D'Alembert")
      plt.plot(henri, label="Labouchère")
      plt.title("Always Banker")
      plt.ylabel("Total ($)")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(222)
      plt.plot(dunce_p, label="Donkey")
      plt.plot(john_p, label="Martingale")
      plt.plot(leonardo_p, label="Fibonacci")
      plt.plot(lucius_p, label="Paroli")
      plt.plot(jean_p, label="D'Alembert")
      plt.plot(henri_p, label="Labouchère")
      plt.title("Always Player")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(223)
      plt.plot(dunce_w, label="Donkey")
      plt.plot(john_w, label="Martingale")
      plt.plot(leonardo_w, label="Fibonacci")
      plt.plot(lucius_w, label="Paroli")
      plt.plot(jean_w, label="D'Alembert")
      plt.plot(henri_w, label="Labouchère")
      plt.title("Always Winner")
      plt.ylabel("Total ($)")
      plt.xlabel("Deals")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(224)
      plt.plot(dunce_c, label="Donkey")
      plt.plot(john_c, label="Martingale")
      plt.plot(leonardo_c, label="Fibonacci")
      plt.plot(lucius_c, label="Paroli")
      plt.plot(jean_c, label="D'Alembert")
      plt.plot(henri_c, label="Labouchère")
      plt.title("Always Chop")
      plt.xlabel("Deals")
      plt.ylim([0, 2500])
      plt.legend()
      plt.tight_layout(pad=1)
      plt.savefig('public/standard_plays.png')

    if args.strategy == "all" or args.strategy == "reverse":
      if args.print:
        print("generating reverse strategy chart...")
      plt.figure(figsize=(16, 8))
      plt.subplot(221)
      plt.plot(ecnud, label="Donkey")
      plt.plot(nhoj, label="Martingale")
      plt.plot(odranoel, label="Fibonacci")
      plt.plot(suicul, label="Paroli")
      plt.plot(naej, label="D'Alembert")
      plt.plot(irneh, label="Labouchère")
      plt.title("Reverse Always Banker")
      plt.ylabel("Total ($)")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(222)
      plt.plot(ecnud_p, label="Donkey")
      plt.plot(nhoj_p, label="Martingale")
      plt.plot(odranoel_p, label="Fibonacci")
      plt.plot(suicul_p, label="Paroli")
      plt.plot(naej_p, label="D'Alembert")
      plt.plot(irneh_p, label="Labouchère")
      plt.title("Reverse Always Player")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(223)
      plt.plot(ecnud_w, label="Donkey")
      plt.plot(nhoj_w, label="Martingale")
      plt.plot(odranoel_w, label="Fibonacci")
      plt.plot(suicul_w, label="Paroli")
      plt.plot(naej_w, label="D'Alembert")
      plt.plot(irneh_w, label="Labouchère")
      plt.title("Reverse Always Winner")
      plt.ylabel("Total ($)")
      plt.xlabel("Deals")
      plt.ylim([0, 2500])
      plt.legend()
      plt.subplot(224)
      plt.plot(ecnud_c, label="Donkey")
      plt.plot(nhoj_c, label="Martingale")
      plt.plot(odranoel_c, label="Fibonacci")
      plt.plot(suicul_c, label="Paroli")
      plt.plot(naej_c, label="D'Alembert")
      plt.plot(irneh_c, label="Labouchère")
      plt.title("Reverse Always Chop")
      plt.xlabel("Deals")
      plt.ylim([0, 2500])
      plt.legend()
      plt.tight_layout(pad=1)
      plt.savefig('public/reverse_plays.png')

    if args.strategy == "all" or args.strategy == "custom":
      if args.print:
        print("generating custom strategy chart...")
      plt.figure(figsize=(16, 8))
      plt.plot(timmy, label="CEG Dealer School")
      plt.plot(mr_gullible, label="Knightingale")
      plt.plot(henry, label="Henri D'Alembert")
      plt.plot(korntanat, label="Pok's Method")
      plt.plot(lauren, label="Lauren's Method")
      plt.plot(james, label="James Bond")
      plt.ylabel("Total ($)")
      plt.xlabel("Deals")
      plt.ylim([0, 2500])
      plt.legend()
      plt.tight_layout(pad=1)
      plt.savefig('public/custom_plays.png')

  if args.analyze == "json" or args.analyze == "all":
    if args.print:
      print("generating json object...")
    count = 0
    output_data = {}
    while count < len(results["strats"]):
      output_data[results["strats"][count]] = {
        "mean": results["means"][count],
        "max": results["maxs"][count],
        "ups": results["ups"][count],
        "ups_p": results["ups_p"][count],
        "flats": results["flats"][count],
        "flats_p": results["flats_p"][count],
        "downs": results["downs"][count],
        "downs_p": results["downs_p"][count],
        "busts": results["busts"][count],
        "busts_p": results["busts_p"][count]
      }
      count += 1
    output = json.dumps(output_data, indent=4)
    # Writing to sample.json
    with open("public/data.js", "w") as outfile:
        outfile.write("let data = " + output + ";")

  if args.analyze == "csv" or args.analyze == "all":
    if args.print:
      print("generating csv file...")
    count = 0
    output_data = []
    # field names  
    fields = [
      'strats', 'mean', 'max', 'ups', 'ups_p', 'flats',
      'flats_p', 'downs', 'downs_p', 'busts', 'busts_p'
    ]
    while count < len(results["strats"]):
      output_data.append([
        results["strats"][count],
        results["means"][count],
        results["maxs"][count],
        results["ups"][count],
        results["ups_p"][count],
        results["flats"][count],
        results["flats_p"][count],
        results["downs"][count],
        results["downs_p"][count],
        results["busts"][count],
        results["busts_p"][count]
      ])
      count += 1
    output = json.dumps(output_data, indent=4)
    # Writing to csv file
    with open("public/data.csv", "w") as outfile:
      # creating a csv writer object
      csvwriter = csv.writer(outfile)
      # writing the fields
      csvwriter.writerow(fields)
      # writing the data rows
      csvwriter.writerows(output_data)