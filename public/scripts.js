function clone (src) {
  return JSON.parse(JSON.stringify(src));
}

function add_trailing_zeros( num ) {
  var value = Number(num);
  value = value.toFixed(2);
  return value
}

function decimal_count(num) {
  const converted = num.toString();
  if (converted.includes('.')) {
     return converted.split('.')[1].length;
  };
  return 0;
}

function create_column (strategy, data) {
  let col = document.createElement("div");
  let title = document.createElement("h3");
  let data_table = document.createElement("table");
  let table_head = document.createElement("thead");
  let table_body = document.createElement("tbody");
  let table_heading = document.createElement("tr");

  let heading_labels = ["Bet", "Average", "Max", "Up", "Flat", "Down", "Bust"];
  let headings = [];
  for (i = 0; i < heading_labels.length; i++) {
    headings[i] = document.createElement("th");
    headings[i].setAttribute("scope", "col");
    headings[i].innerHTML = heading_labels[i];
    table_heading.appendChild(headings[i]);
  }
  for (let bet in data) {
    table_row = document.createElement("tr");
    bet_label = document.createElement("th");
    bet_label.setAttribute("scope", "row");
    bet_label.innerHTML = bet;
    mean = document.createElement("td");
    if (decimal_count(data[bet]["mean"]) < 2) {
      data[bet]["mean"] = add_trailing_zeros(data[bet]["mean"])
    }
    if (decimal_count(data[bet]["max"]) < 2) {
      data[bet]["max"] = add_trailing_zeros(data[bet]["max"])
    }
    if (decimal_count(data[bet]["ups_p"]) < 2) {
      data[bet]["ups_p"] = add_trailing_zeros(data[bet]["ups_p"])
    }
    if (decimal_count(data[bet]["flats_p"]) < 2) {
      data[bet]["flats_p"] = add_trailing_zeros(data[bet]["flats_p"])
    }
    if (decimal_count(data[bet]["downs_p"]) < 2) {
      data[bet]["downs_p"] = add_trailing_zeros(data[bet]["downs_p"])
    }
    if (decimal_count(data[bet]["busts_p"]) < 2) {
      data[bet]["busts_p"] = add_trailing_zeros(data[bet]["busts_p"])
    }
    mean.innerHTML = "$" + data[bet]["mean"];
    max = document.createElement("td");
    max.innerHTML = "$" + data[bet]["max"];
    ups = document.createElement("td");
    ups.innerHTML = data[bet]["ups_p"] + "%";
    flats = document.createElement("td");
    flats.innerHTML = data[bet]["flats_p"] + "%";
    downs = document.createElement("td");
    downs.innerHTML = data[bet]["downs_p"] + "%";
    busts = document.createElement("td");
    busts.innerHTML = data[bet]["busts_p"] + "%";
    table_row.appendChild(bet_label);
    table_row.appendChild(mean);
    table_row.appendChild(max);
    table_row.appendChild(ups);
    table_row.appendChild(flats);
    table_row.appendChild(downs);
    table_row.appendChild(busts);
    table_body.appendChild(table_row);
  }
  col.className = "col-6";
  data_table.className = "table table-striped table-sm fs-7";
  title.innerHTML = strategy;

  table_head.appendChild(table_heading);

  data_table.appendChild(table_head);
  data_table.appendChild(table_body);

  col.appendChild(title);
  col.appendChild(data_table);

  return col;
};

function create_row (label, data) {
  table_row = document.createElement("tr");
  row_label = document.createElement("th");
  row_label.setAttribute("scope", "row");
  row_label.innerHTML = label;
  if (decimal_count(data["mean"]) < 2) {
    data["mean"] = add_trailing_zeros(data["mean"])
  }
  if (decimal_count(data["max"]) < 2) {
    data["max"] = add_trailing_zeros(data["max"])
  }
  if (decimal_count(data["ups_p"]) < 2) {
    data["ups_p"] = add_trailing_zeros(data["ups_p"])
  }
  if (decimal_count(data["flats_p"]) < 2) {
    data["flats_p"] = add_trailing_zeros(data["flats_p"])
  }
  if (decimal_count(data["downs_p"]) < 2) {
    data["downs_p"] = add_trailing_zeros(data["downs_p"])
  }
  if (decimal_count(data["busts_p"]) < 2) {
    data["busts_p"] = add_trailing_zeros(data["busts_p"])
  }
  mean = document.createElement("td");
  mean.innerHTML = "$" + data["mean"];
  max = document.createElement("td");
  max.innerHTML = "$" + data["max"];
  ups = document.createElement("td");
  ups.innerHTML = data["ups_p"] + "%";
  flats = document.createElement("td");
  flats.innerHTML = data["flats_p"] + "%";
  downs = document.createElement("td");
  downs.innerHTML = data["downs_p"] + "%";
  busts = document.createElement("td");
  busts.innerHTML = data["busts_p"] + "%";
  table_row.appendChild(row_label);
  table_row.appendChild(mean);
  table_row.appendChild(max);
  table_row.appendChild(ups);
  table_row.appendChild(flats);
  table_row.appendChild(downs);
  table_row.appendChild(busts);
  return table_row;
};

let strategies = {
  "Donkey": {},
  "Martingale": {},
  "Fibonacci": {},
  "Paroli": {},
  "D'Alembert": {},
  "Labouchère": {},
  "Reverse Donkey": {},
  "Reverse Martingale": {},
  "Reverse Fibonacci": {},
  "Reverse Paroli": {},
  "Reverse D'Alembert": {},
  "Reverse Labouchère": {}
}
let customs = {
  "CEG Dealer School": {},
  "Knightingale": {},
  "Henri D'Alembert": {},
  "Pok's Method": {},
  "Lauren's Method": {},
  "James Bond": {}
};

for (entry in data) {
  if (entry.includes("(")) {
    strategies[
      entry.substring(0, entry.indexOf("(") - 1)
    ][
      entry.substring(entry.indexOf("(") + 1, entry.indexOf(")"))
    ] = {
      "mean": data[entry]["mean"],
      "max": data[entry]["max"],
      "ups": data[entry]["ups"],
      "ups_p": data[entry]["ups_p"],
      "flats": data[entry]["flats"],
      "flats_p": data[entry]["flats_p"],
      "downs": data[entry]["downs"],
      "downs_p": data[entry]["downs_p"],
      "busts": data[entry]["busts"],
      "busts_p": data[entry]["busts_p"]
    }
  } else {
    customs[entry] = {
      "mean": data[entry]["mean"],
      "max": data[entry]["max"],
      "ups": data[entry]["ups"],
      "ups_p": data[entry]["ups_p"],
      "flats": data[entry]["flats"],
      "flats_p": data[entry]["flats_p"],
      "downs": data[entry]["downs"],
      "downs_p": data[entry]["downs_p"],
      "busts": data[entry]["busts"],
      "busts_p": data[entry]["busts_p"]
    }
  }
}

standarddiv = document.getElementById("standarddiv");
reversediv = document.getElementById("reversediv");
customdiv = document.getElementById("customdiv");

for (strategy in strategies) {
  let table = create_column(strategy, strategies[strategy]);
  if (strategy.includes("Reverse")) {
    reversediv.appendChild(table);
  } else {
    standarddiv.appendChild(table);
  }
}

let data_table = document.createElement("table");
let table_head = document.createElement("thead");
let table_body = document.createElement("tbody");
let table_heading = document.createElement("tr");

let heading_labels = ["Bet", "Average", "Max", "Up", "Flat", "Down", "Bust"];
let headings = [];
for (i = 0; i < heading_labels.length; i++) {
  headings[i] = document.createElement("th");
  headings[i].setAttribute("scope", "col");
  headings[i].innerHTML = heading_labels[i];
  table_heading.appendChild(headings[i]);
}

for (custom in customs) {
  let table_row = create_row(custom, customs[custom]);
  table_body.appendChild(table_row);
}

data_table.className = "table table-striped table-sm fs-7";

table_head.appendChild(table_heading);

data_table.appendChild(table_head);
data_table.appendChild(table_body);
customdiv.appendChild(data_table);

let = num_decks = 8; //document.getElementById('num-decks').value

const SUITS = ["\u2663", "\u2665", "\u2666", "\u2660"];
const RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
function get_deck () {
	let deck = new Array();
	for (let i = 0; i < SUITS.length; i++) {
		for (let x = 0; x < RANKS.length; x++) {
			let card = {value: RANKS[x], suit: SUITS[i]};
			deck.push(card);
		}
	}
	return deck;
}

function get_value (cards) {
  total = 0;
  let rank_values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
  if (cards.constructor === Array) {
    for (card in cards) {
      total += rank_values[RANKS.indexOf(cards[card].value)];
    }
  } else {
    total = rank_values[RANKS.indexOf(cards.value)];
  }
  while (total >= 10) {
    total = total - 10;
  }
  return total;
}

function shuffle (deck) {
	// for 1000 turns
	// switch the values of two random cards
	for (let i = 0; i < 1000; i++) {
		let location1 = Math.floor((Math.random() * deck.length));
		let location2 = Math.floor((Math.random() * deck.length));
		let tmp = deck[location1];
		deck[location1] = deck[location2];
		deck[location2] = tmp;
	}
}

function get_max(a){
  return Math.max(...a.map(e => Array.isArray(e) ? get_max(e) : e));
}

function get_min(a){
  return Math.min(...a.map(e => Array.isArray(e) ? get_min(e) : e));
}

function factorialize(num) {
  if (num < 0) 
        return -1;
  else if (num == 0) 
      return 1;
  else {
      return (num * factorialize(num - 1));
  }
}

function combination (N, R) {
  return factorialize(N) / (factorialize(R) * factorialize(N-R));
}

function hypergeo_distribution (X, Y, Z, N) {
  // X is the number of a certain card in the deck
  // Y is the total number of cards in the deck
  // Z is the number of cards drawn
  // N is the number of cards you are checking for
  xCn = combination(X, N);
  yxCzn = combination(Y-X, Z-N);
  yCz = combination(Y,Z);
  return (xCn * yxCzn) / yCz;
}

function probability_sequence (series, ...sequence) {
  probabilities = [];
  for (number in sequence) {

  }
}

function colorize (value, minimum, maximum) {
  let norm = (value - minimum) / (maximum - minimum);
  // let r = Math.round(norm * minRed   + (1 - norm) * maxRed);
  // let g = Math.round(norm * minGreen + (1 - norm) * maxGreen);
  // let b = Math.round(norm * minBlue  + (1 - norm) * maxBlue);
  let hue = (norm * 120).toString(10);
  return ["hsl(", hue, ",100%,50%)"].join("");
}

function generate_probability_table (odds) {
  let column_totals = [];
  let table = document.getElementById("predictor-table");
  let header = document.createElement("thead");
  let body = document.createElement("tbody");
  let footer = document.createElement("tfoot");
  let table_header = document.createElement("tr");
  let table_footer = document.createElement("tr");

  let max_odd = get_max(odds);
  let min_odd = get_min(odds);

  let heading = document.createElement("th");
  heading.setAttribute("scope", "col");
  heading.innerHTML = "#";
  table_header.appendChild(heading);
  for (i = 0; i < 10; i++) {
    column_totals.push(0);
    heading = document.createElement("th");
    heading.setAttribute("scope", "col");
    heading.innerHTML = i;
    table_header.appendChild(heading);
  }
  heading = document.createElement("th");
  heading.setAttribute("scope", "col");
  heading.innerHTML = "Total";
  table_header.appendChild(heading);

  for (i = 0; i < odds.length; i++) {
    let row_total = 0;
    let table_body_row = document.createElement("tr");
    let row_start = document.createElement("th");
    let row_end = document.createElement("th");
    row_start.setAttribute("scope", "row");
    row_start.innerHTML = i;
    table_body_row.appendChild(row_start);
    for (j = 0; j < odds[i].length; j++) {
      row_total += parseFloat(odds[i][j]);
      column_totals[j] += parseFloat(odds[i][j]);
      let row_entry = document.createElement("td");
      row_entry.innerHTML = String(parseFloat(odds[i][j]).toFixed(3)) + "%";
      row_entry.style.color=colorize(parseFloat(odds[i][j]), min_odd, max_odd);
      table_body_row.appendChild(row_entry);
    }
    row_end.innerHTML = String(row_total.toFixed(2)) + "%";
    table_body_row.appendChild(row_end);
    body.appendChild(table_body_row);
  }

  let total_totals = 0;
  let footing = document.createElement("th");
  footing.setAttribute("scope", "col");
  footing.innerHTML = "Total";
  table_footer.appendChild(footing);
  for (i = 0; i < 10; i++) {
    total_totals += parseFloat(column_totals[i]);
    footing = document.createElement("th");
    footing.setAttribute("scope", "col");
    footing.innerHTML = String(parseFloat(column_totals[i]).toFixed(2)) + "%";
    table_footer.appendChild(footing);
  }
  footing = document.createElement("th");
  footing.setAttribute("scope", "col");
  footing.innerHTML = String(total_totals.toFixed(0)) + "%";
  table_footer.appendChild(footing);

  header.appendChild(table_header);
  footer.appendChild(table_footer);

  table.innerHTML = "";
  table.appendChild(header);
  table.appendChild(body);
  table.appendChild(footer);
}

function get_odds (shoe) {
  let num_cards = [];
  let values = [];
  const counts = {};
  for (card in shoe) {
    values.push(get_value(shoe[card]));
    let result = shoe.filter(found_card => {
      return found_card.value === shoe[card].value && found_card.suit === shoe[card].suit
    })
    if (result === undefined) {
      num_cards.push({value: shoe[card].value, suit: shoe[card].suit, num: 1});
    } else {
      result.num += 1;
    }
  }
  console.log(num_cards);

  for (const card of values) {
    counts[card] = counts[card] ? counts[card] + 1 : 1;
  }
  console.log(counts);

  let base_odds = [];
  for (i = 0; i < 10; i++) {
    let column_odds = [];
    for (j = 0; j < 10; j++) {
      let odd = (counts[i] / values.length) * (counts[j] / (values.length - 1)) * 100;
      if (isNaN(odd)) {
        odd = 0;
      }
      column_odds.push(odd);
    }
    base_odds.push(column_odds);
  }
  // console.log(base_odds);
  generate_probability_table(base_odds);

  // https://www.pinnacle.com/en/betting-articles/casino/baccarat-side-bets-explained/h3h25hnfeqczzsvj
  // All Red / All Black | all red: 22:1, all black: 24:1
  // Bellagio Match (three of a kind) | banker: 68:1, player: 75:1
  // let bellagio_match = combination() * combination() * combination() * 100
  // Big and Small / 4-5-6 (number of ending cards) | 4: 3:2, 5/6: 2:1
  // Double 8 (tie @ 8): 15:1
  // Dragon 7 (banker win at 3 card 7): 40:1
  // Super 6 (banker win at 6): 12:1
  // Lucky Bonus (banker win at 6 + banker bet): 18:1 and < banker bet
  // Lucky / Unlucky 8 (win/lose at 8): 4:1
  // Panda 8 (player win at 3 card 8): 25:1
  // Perfect Pair (dealt pair) | ranks: 5:1, ranks + suits: 25:1
  // Royal Match (King + Queen) | unlike suit: 30:1, like suit: 75:1
  // Same Suit Deal | player: 2.87:1, banker: 2.86:1
  // Suited three-card 8 | either: 25:1, both: 200:1
  // Three-card 6 | either: 8:1, both: 100:1
  // Value bets
  //   >9.5: 1.66:1, <9.5: 2.23:1
  //   Odd: 1.92:1, Even: 1.91:1
  //   Quik: [0: 50:1, 1-3: 1:1, 15-17: 1:1, 18: 25:1]
  // Matching Dragon (number of specific rank cards dealt)
  //   total 1: 1:1
  //   total 2: 3:1
  //   total 3: 20:1
  //   total 4: 40:1
  //   total 5: 60:1
  //   total 6: 100:1
  // Dragon Bonus (natural banker win with margin)
  //   margin 4: 1:1
  //   margin 5: 2:1
  //   margin 6: 4:1
  //   margin 7: 6:1
  //   margin 8: 10:1
  //   margin 9: 30:1
  // Egalite bets
  //   tie 9: 80:1
  //   tie 8: 80:1
  //   tie 7: 45:1
  //   tie 6: 45:1
  //   tie 5: 110:1
  //   tie 4: 120:1
  //   tie 3: 200:1
  //   tie 2: 225:1
  //   tie 1: 215:1
  //   tie 0: 150:1
}

let shoe = [];
for (i = 0; i < num_decks; i++) {
  shoe = shoe.concat(get_deck());
  shuffle(shoe);
}

get_odds(shoe);

document.getElementById('remove_cards').onclick = function() {
  card_suits = ["C", "H", "D", "S"];
  for (i = 0; i < 6; i++) {
    let card_rank = document.getElementById('card_' + i + '_rank').value;
    let card_suit = SUITS[card_suits.indexOf(document.getElementById('card_' + i + '_suit').value)];
    let idx = shoe.findIndex(card => card.value == card_rank && card.suit == card_suit);
    if (idx >= 0) { //check if item found
      var removed = shoe.splice(idx, 1);
      console.log("removed: " + removed);
    } else if (i != 2 && i != 5) {
      console.log('Not found: ' + card_rank + " of " + card_suit);
    }
  }
  get_odds(shoe);
};