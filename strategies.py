import numpy as np

def get_prior_winner(scores, now, priors=1):
  result = None
  then = 1
  cur_scores = scores[:now]
  while result is None:
    if len(cur_scores) > then and now - then > 0:
      if cur_scores[now - then] == "tie":
        then += 1
      else:
        if priors - 1 <= 0:
          result = cur_scores[now - then]
        else:
          priors -= 1
    else:
      result = "banker" # defer to banker
  return result

def chop_run(scores, position, runs):
  chops = 0
  priors = []
  chop = False
  for run in range(runs):
    priors.append(get_prior_winner(scores, position, priors=run))
  for run in range(runs):
    if len(priors) > 1:
      if priors[0] != priors[1]:
        chops += 1
    del priors[0]
  if chops >= runs: chop = True
  return chop

def fibcalc(n):
    if n < 0:
        print("fibcalc error")
    elif n == 0:
        return 0
    elif n == 1 or n == 2:
        return 1
    else:
        return fibcalc(n-1) + fibcalc(n-2)

# always plays same bet
def donkey(scores, total, minimum, pose="banker"):
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum
    if scores[cur_game] == pose:
      total += bet
    elif scores[cur_game] == oppose:
      total -= bet
    results.append(total)
    cur_game += 1
  return results

# double prior bet per loss
def martingale(scores, total, minimum, pose="banker"):
  losses = 0
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * (2**losses)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      losses = 0
    elif scores[cur_game] == oppose:
      total -= bet
      losses += 1
    results.append(total)
    cur_game += 1
  return results

# bet to fibonacci sequence per loss
def fibonacci(scores, total, minimum, pose="banker"):
  losses = 1 # you're a loser for trying
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * fibcalc(losses)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      losses = 1
    elif scores[cur_game] == oppose:
      total -= bet
      losses += 1
    results.append(total)
    cur_game += 1
  return results

# double prior bet per win
# reset bet after 3 wins
def paroli(scores, total, minimum, pose="banker"):
  wins = 0
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * (2**wins)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      wins += 1
    elif scores[cur_game] == oppose:
      total -= bet
      wins = 0
    if wins > 3: wins = 1
    results.append(total)
    cur_game += 1
  return results

# increase bet by base on loss
# decrease bet by base on win
def d_alembert(scores, total, minimum, pose="banker", base=None):
  results = []
  cur_game = 0
  bet = minimum
  results.append(total) # start with buy in
  if base is None: base = minimum
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      if bet > base: bet -= base
    elif scores[cur_game] == oppose:
      total -= bet
      bet += base
    results.append(total)
    cur_game += 1
  return results

# compile sequence that sums to goal
# bet sum of first and last of sequence
# remove first and last of sequence on win
# add summed number to end of sequence on loss
def labouchere(scores, total, minimum, pose="banker", goal=500, sequence_length=10):
  def seqgen(max, length):
    seq = np.random.randint(low=1, high=max, size=length)
    total = np.sum(seq)
    seq = seq / total
    seq = seq * goal
    return seq
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  sequence = seqgen(goal, sequence_length)
  new_seq = sequence.copy()
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    if len(new_seq) == 0:
      new_seq = sequence.copy()
      bet = new_seq[0] + new_seq[-1]
    elif len(new_seq) < 2:
      bet = new_seq[0]
    else:
      bet = new_seq[0] + new_seq[-1]
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      if len(new_seq) >= 2:
        np.delete(new_seq, 0)
        np.delete(new_seq, -1)
      else:
        np.delete(new_seq, 0)
    elif scores[cur_game] == oppose:
      total -= bet
      np.append(sequence, bet)
      new_seq = sequence.copy()
    results.append(total)
    cur_game += 1
  return results

# double prior bet per win
def reverse_martingale(scores, total, minimum, pose="banker"):
  wins = 0
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * (2**wins)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      wins += 1
    elif scores[cur_game] == oppose:
      total -= bet
      wins = 0
    results.append(total)
    cur_game += 1
  return results

# bet to fibonacci sequence per win
def reverse_fibonacci(scores, total, minimum, pose="banker"):
  wins = 1
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * fibcalc(wins)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      wins += 1
    elif scores[cur_game] == oppose:
      total -= bet
      wins = 1
    results.append(total)
    cur_game += 1
  return results

# double prior bet per loss
# reset bet after 3 losses
def reverse_paroli(scores, total, minimum, pose="banker"):
  losses = 0
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * (2**losses)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      losses = 0
    elif scores[cur_game] == oppose:
      total -= bet
      losses += 1
    if losses > 3: losses = 0
    results.append(total)
    cur_game += 1
  return results

# increase bet by base on win
# decrease bet by base on loss
def reverse_d_alembert(scores, total, minimum, pose="banker", base=None):
  results = []
  cur_game = 0
  bet = minimum
  results.append(total) # start with buy in
  if base is None: base = minimum
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      bet += base
    elif scores[cur_game] == oppose:
      total -= bet
      if bet > base: bet -= base
    results.append(total)
    cur_game += 1
  return results

# compile sequence that sums to goal
# bet sum of first and last of sequence
# remove first and last of sequence on loss
# add summed number to end of sequence on win
def reverse_labouchere(scores, total, minimum, pose="banker", goal=500, sequence_length=10):
  def seqgen(max, length):
    seq = np.random.randint(low=1, high=max, size=length)
    total = np.sum(seq)
    seq = seq / total
    seq = seq * goal
    return seq
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  sequence = seqgen(goal, sequence_length)
  new_seq = sequence.copy()
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    if len(new_seq) == 0:
      new_seq = sequence.copy()
      bet = new_seq[0] + new_seq[-1]
    elif len(new_seq) < 2:
      bet = new_seq[0]
    else:
      bet = new_seq[0] + new_seq[-1]
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      np.append(sequence, bet)
      new_seq = sequence.copy()
    elif scores[cur_game] == oppose:
      total -= bet
      if len(new_seq) >= 2:
        np.delete(new_seq, 0)
        np.delete(new_seq, -1)
      else:
        np.delete(new_seq, 0)
    results.append(total)
    cur_game += 1
  return results

# don't play first 3 hands
# bet prior winner unless 3 chop then bet chop
# if win skip next deal
# if loss martingale base 4 times then return to base
def ceg_dealer_school(scores, total, minimum, base=None):
  if base is None: base = minimum
  bet = base
  results = []
  cur_game = 0
  martingales = 0
  results.append(total) # start with buy in
  didnt_win_prior = True
  while total >= minimum and cur_game < len(scores):
    if cur_game > 2: # skip first 3
      if didnt_win_prior: # skip a deal after win
        prior = get_prior_winner(scores, cur_game)
        if chop_run(scores, cur_game, 3): # chop run of 3
          pose = "banker" if prior == "player" else "player"
        else:
          pose = prior
        oppose = "banker" if pose == "player" else "player"
        if bet > total: bet = total
        if scores[cur_game] == pose:
          total += bet
          bet = base
          didnt_win_prior = False
        elif scores[cur_game] == oppose:
          total -= bet
          if martingales <= 4: # martingale up to 4 times on losses
            bet = base * (2**martingales)
          else:
            martingales = 0
            bet = base
      else:
        didnt_win_prior = True
    results.append(total)
    cur_game += 1
  return results

# double prior bet per streak
def knightingale(scores, total, minimum, pose="banker"):
  streak = 0
  results = []
  cur_game = 0
  prior_win = False
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if pose in ["winner", "chop"]:
      prior = get_prior_winner(scores, cur_game)
      if pose == "winner":
        pose = prior
      else:
        pose = "banker" if prior == "player" else "player"
    oppose = "banker" if pose == "player" else "player"
    bet = minimum * (2**streak)
    if bet > total: bet = total
    if scores[cur_game] == pose:
      total += bet
      if prior_win: streak += 1
      else: streak = 0
      prior_win = True
    elif scores[cur_game] == oppose:
      total -= bet
      if not prior_win: streak += 1
      else: streak = 0
      prior_win = False
    results.append(total)
    cur_game += 1
  return results

# don't play first 3 hands
# bet prior winner unless 3 chop then bet chop
# if win reverse d'alembert
# if loss bet to fibonacci sequence per loss up to 4 times
def henri_d_alembert(scores, total, minimum, base=None):
  results = []
  cur_game = 0
  bet = minimum
  losses = 0
  results.append(total) # start with buy in
  didnt_win_prior = True
  if base is None: base = minimum
  while total >= minimum and cur_game < len(scores):
    if cur_game > 2: # skip first 3
      if didnt_win_prior: # skip a deal after win
        pose = "banker"
        oppose = "banker" if pose == "player" else "player"
        if bet > total: bet = total
        if scores[cur_game] == pose:
          total += bet
          bet += base
          losses = 0
          didnt_win_prior = False
        elif scores[cur_game] == oppose:
          total -= bet
          if losses <= 3: losses += 1
          else: losses = 0
          bet = minimum * fibcalc(losses)
      else:
        didnt_win_prior = True
    results.append(total)
    cur_game += 1
  return results

# don't play first 3 hands
# bet prior winner unless 3 chop then bet chop
# if win skip next deal add base bet
# if loss return to base
def pok(scores, total, minimum, base=None):
  if base is None: base = minimum
  bet = base
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  didnt_win_prior = True
  while total >= minimum and cur_game < len(scores):
    if cur_game > 2: # skip first 3
      if didnt_win_prior: # skip a deal after win
        prior = get_prior_winner(scores, cur_game)
        if chop_run(scores, cur_game, 3): # chop run of 3
          pose = "banker" if prior == "player" else "player"
        else:
          pose = prior
        oppose = "banker" if pose == "player" else "player"
        if bet > total: bet = total
        if scores[cur_game] == pose:
          total += bet
          bet += base
          didnt_win_prior = False
        elif scores[cur_game] == oppose:
          total -= bet
          bet = base
          #if martingales <= 4: # martingale up to 4 times on losses
          #  bet = base * (2**martingales)
          #else:
          #  martingales = 0
          #  bet = base
      else:
        didnt_win_prior = True
    results.append(total)
    cur_game += 1
  return results

  # if winner == "banker":
  #   print("->BANKER")
  # elif winner == "player":
  #   print("->PLAYER")
  # elif winner == "tie":
  #   print("->TIE")

# start with banker 1 unit
# bet winner every hand after
# bet tie for first 3 hands
# each tie bet tie twice thereafter
# bet fibonacci each win run until 3 units
def laurens_method(scores, total, minimum):
  wins = 0
  results = []
  cur_game = 0
  tie_bet = True
  tie_bet_val = 0.4 * minimum
  results.append(total) # start with buy in
  while total >= minimum and cur_game < len(scores):
    if cur_game < 3 or scores[cur_game - 1] == "tie" or scores[cur_game - 2] == "tie" or cur_game == len(scores) - 1:
      tie_bet = True
    else:
      tie_bet = False
    if cur_game == 0:
      pose = "banker"
    else:
      pose = get_prior_winner(scores, cur_game)
    oppose = "banker" if pose == "player" else "player"
    bet = minimum
    if scores[cur_game] == pose:
      total += bet
      if wins < 3: wins += 1
      bet = minimum * wins
      if tie_bet:
        total -= tie_bet_val
    elif scores[cur_game] == oppose:
      total -= bet
      wins = 0
      if tie_bet:
        total -= tie_bet_val
    elif scores[cur_game] == "tie":
      if tie_bet:
        total += 8 * tie_bet_val
      tie_bet = True
    results.append(total)
    cur_game += 1
  return results

# always same bet
# one unit on player
# 3 units on banker
# 1/5 unit on tie
def james_bond(scores, total, minimum):
  results = []
  cur_game = 0
  results.append(total) # start with buy in
  banker_bet = 3 * minimum
  player_bet = minimum
  tie_bet = minimum / 5
  while total >= minimum and cur_game < len(scores):
    if scores[cur_game] == "banker":
      total += banker_bet
      total -= player_bet
      total -= tie_bet
    elif scores[cur_game] == "player":
      total -= banker_bet
      total += player_bet
      total -= tie_bet
    elif scores[cur_game] == "tie":
      total += 8 * tie_bet
    results.append(total)
    cur_game += 1
  return results
